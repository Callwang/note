## 常用组合键
>[Tab] [Tab] 命令补全/文件补齐/命令参数提示  

>[Ctrl] + c 中断目前的程序  

>[Ctrl] + d 相当于exit  

>[Ctrl] + z 暂停程序  

>[Ctrl] + r 历史命令搜索  
## 命令说明
>[xxx --help] 命令的简单介绍以及该命令的可选参数  

>[man xxx] 命令的详细说明  

>[info xxx] 类似网页的形式显示命令的详细说明（命令说明文件要有info格式的）  
## 重启与关机
>reboot & shutdown -r now 重启  

>shutdown -h now & poweroff & halt 关机  

>sync 数据同步写入到磁盘  
>重启和关机可时sync将内存中的数据同步到磁盘中  sync; reboot & sync; shutdown -h now  
## 目录相关操作
>cd  -  切换会上次操作目录  

>pwd [-P] 显示当前目录 带参数P会列出实际目录而非链接目录 

>mkdir/rmdir [-p] 创建目录/删除目录 带参数p会递归创建或递归删除  

>cp/rm/mv 复制/删除/移动  

>basename/dirname 获路径的文件名/获取目录名  
## 文件内容查看  
>cat [-n] 由第一行开始显示文件内容  

>tac 从最后一行开始显示  

>nl 显示的时候，同时显示行号 [-b a]显示空行的行号  

>more 一页一页的看  

>less 与more类似，但它可以往前翻页 [/][?]向下和向上查找 

>head 只看前面几行  

>tail [-n] [-f] 只看后几行  

>cat -n /file | head -n 20 | tail -n 10  截取查看第11到第20行  

>touch 创建新文件  

>file 观察文件类型  
## 命令与文件的查找  
>whereis 查找命令的执行文件(默认找PATH设置的目录 通过[-l]可以查看搜索的目录)  

>find [PATH] [option] [action]   
>与时间有关的选项  
>- -mtime n :n为数字，意义为在n天之前的[一天之内]被修改过内容的文件   
>- -mtime +n :列出在n天之前的[不含n天本身]被修改过内容的文件  
>- -mtime -n :列出在n天之内的[含n天本身]被修改过内容的文件   
>- -newer file : file为一个存在的文件，列出比file还要新的文件  

>find / -mtime 0 0代表现在的时间，从现在开始到24小时前  
>find / -mtime 3 三天前那一天24小时内被修改过的文件  
>find /etc -newer /etc/passwd  在/etc下查找比/etc/passwd更新的文件  
>查找与名称有关的参数  
>- -name 要查找的文件名称  
>- -size [+-]size 查找比size(+)大或(-)小的文件  
>- -type type 查找文件类型为type的，主要有正规文件f 设备文件b,c 目录d 链接文件l 等  
>find / -name "passwd" 根据关键字查找

>额外操作  
>- -exec command 可接额外的命令来处理查找结果(command 不能是别名)  
>- -print 将结果打印到屏幕,默认操作  

>find / -name "passwd" -exec ls -l {} ;  
## 文件系统的操作  
>df 列出文件系统的整体磁盘使用量 

>df -h /etc 将/etc下的磁盘容量结果以易读的格式显示出来  

>du 列出目前目录下的所有文件容量  

>du -ah --max-depth=1 检查当前目录下的各文件或文件夹所占用的容量  
>-    -a 列出所有文件与目录的容量  
>-    -h 已易读的格式(M/G)格式显示  
>-    ---max-depth 遍历文件夹深度  
## vim编辑器
>vim file1 file2  同时编辑多文件  
>:files 查看已打开的文件  
>:n 编辑下一个文件  
>:N 编辑上一个文件  
>gg 移动到这个文件的第一行  
>G 移动到这个文件的最后一行  
>/word 光标之下查找字符串  
>?word 光标之上查找字符串  
>ndd 删除光标所在的向下的n行  
>nyy 复制光标所在的向下的n行  
>p 将已复制的数据在光标下一行粘贴 P为粘贴光标上一行  
>u 恢复上一个操作  
>[ctrl] + r 重复上一个操作  
>:set nu|nonu 显示/取消行号  
>:sp filename 将一个文件显示在一个窗口中或者是在窗口中显示另外一个文件  
>[ctrl]+w+向上 光标移动到上面的窗口  
>[ctrl]+w+向下 光标移动到下面的窗口  
## Shell命令
>登录后的shell 一般会有6个  用ctrl+alt+F1~F6进行shell切换  

>history 历史命令 ~/.bash_history中只记录上次登录前的，本次登录的还在内存中  

>history n 最近n条命令  

>history -c 清除所有历史操作记录  

>history -w 将内存中的历史命令记录到文件中  

>!number 执行第几条命令  

>!command 由最近的命令向前查找以command开头的那个命令 并执行  

>!! 执上一条命令  

>alias 命令别名的设置 alias jump='ssh jumpServer'  然后可通过type查看 用unalias取消别名设置  

>type 查询命令是否为Shell的内置命令(命令的执行目录)  

>变量设置 name=wang 变量名只能时字母或数字，开头不能是数字  
>变量扩充 
```
name="name"yonghui 或 name=${name}yonghui  
```
>name双引号内的可保持原有的特性 单引号中的$会被解释成纯文本  

>echo 读取变量内容  echo   

>export 变量名 让变量成为环境变量(子程序看可以取到)  

>unset 取消变量的设置    

>read 读取键盘输入的变量[-p 提示字符][-t 等待时间(秒)] read -p "please keyin >you name:" -t 30 name  

>source /etc/profile  . /etc/profile  读入环境配置文件(用户个人的配置文件[/.bashrc]/[/.bash_profile]/[~/.profile])  
>~/.bash_logout 退出登录前需要做的一些操作配置在这里面  
## 流的重定向
>   1> 用覆盖的方式将正确的数据输出到指定的文件上 如果仅存在>时  默认就是1>  

>   1>> 用追加的方式将正确的数据输出到指定的文件上 如果仅存在>>时  默认就是1>>  

>   2> 用覆盖的方式将错误的数据输出到指定的文件上  

>   2>> 用追加的方式将错误的数据输出到指定的文件上  

>   2>&1或>& 将正确的和错误的数据都输出到指定的一个文件中  

>   /dev/null 垃圾桶黑洞不需要的数据可以重定向到这里    

>   < 将原本需要由键盘输入的数据，改为由文件内容替换  用ctrl+d结束输入    

>cat > catfile 创建一个文件，文件内容为键盘输入内容  

>cat > catfile < ~/.bashrc 和cp命令测效果一致  

><< 将原本需要由键盘输入的数据，改为由文件内容替换 用指定的字符结束输入

>cat > catfile << "end"  
## 管道命令
>cut 将一行信息的某段内容切出来  
>-    -d 后面跟分隔符，需要与-f 一起使用  
>-    -f 取出用-d分割后的第几段  
```
echo $PATH | cut -d ':' -f 3,5 用:分割取出第三和第五个  
```

>grep [-参数] [--color=auto] '查找字符' filename  
>-    -c 计算找到 '查找字符'的次数  last | grep -c wyh  
>-    -i 忽略大小写  
>-    -n 输出行号  last | grep -n wyh  
>-    -v 反向选择  last | grep -v wyh  
>-    -An 为after意思，除了改行以外的后续n行也列出来  
>-    -Bn 为befer意思，除了改行以外的前面n行也列出来  

>sort [-参数] file  排序  
>- -f 忽略大小写的差异，例如A与a视为编码相同  
>- -b 忽略最前面的空格部分  
>- -M 用月份的名字来排序  
>- -n 使用纯数字进行排序  
>- -r 反向排序  
>- -u 相同的数据仅出现一行  
>- -t 分割符号 默认使用table来分割  
>- -k 用分割后的你个区间来排序   

>cat /etc/passwd | sort -t ":" -k 3 -n    
>last | cut -d ' ' -f 1 | sort -u    
>last | cut -d ' ' -f 1 | sort | uniq -c  
>wc [-参数]   行、字、字符的统计  
>- -l 仅列出行数  
>- -w 仅列出多少字(英文字母)  
>- -m 列出多少字符  
```
wyh@wyh-NBD-WXX9:~/mytest$ last | wc
    274    2765   19991
    行数   字数   字符数
```

>tee [-a] 双向重定向 -a 用追加的方式将数据写入文件中  

>last | tee last.list  屏幕会输出信息，并且屏幕输出的信息会记录到last.list中  

>split [-bl] file  
>- -b 后面可接想要划分的文件大小，可加单位b,k,m  
>- -l 一行数来进行划分  

>last | split -n 5k lasts  
>last | split -l 20 lasts  

>sed 将数据进行替换、删除、新增、选取特定行  
>操作：  
>- a 新增  nl /etc/passwd | sed '5a wang'  
>- c 替换  nl /etc/passwd | sed '2,5c wang'  
>- d 删除  nl /etc/passwd | sed '2,5d'  
>- i 插入  nl /etc/passwd | sed '5i wang'  
>- p 打印  nl /etc/passwd | sed -n '5,7p'  
>- s 替换  nl test.sh | sed '5,7s/a/mmmm/g'  
>直接对原文件进行修改  sed -i '5a wang' test.sh  

>awk 主要是处理每一行的字段内的数据  
>awk '条件类型1 {操作1} 条件类型1 {操作1} ...' filename  
>变量：  
>NF 每一行($0)拥有的字段总数  
>NR 目前awk所处理的第几行数据  
>FS 目前的分隔符，默认是空格键  

```
cat /etc/passwd | awk 'BEGIN{FS=":"} $3>100 {print $1 "\t\t" $3 "\t\tline:" NR "\t\tcount:" NF}'

cat /etc/passwd | awk 'BEGIN{FS=":"} NR%2==0 {print $1 "\t\t" $3 "\t\tline:" NR "\t\tcount
:" NF}'
```
>diff  比较两个文件的差异   
>diff [参数] file1 file2  
>- -b 忽略一行当中仅有多个空白的差异例如"wang"与"w  ang"  
>- -B 忽略空白行  
>- -i 忽略大小写的不同  
## 进程操作  
>& 直接将任务扔到后台运行  

>jobs 查看后台任务的状态  

>- -l 列出任务序号、命令串和PID  

>- -r 仅列出正在 后台运行的任务  

>- -s 仅列出正在后台暂停的任务  

>fg %任务号 将后台的任务拿到前台来处理  

>bg %任务号 将后台暂停的任务变成运行中  

>kill 管理后台中的任务  
>- 1 重启  
>- 9 立即强制删除一个任务  
>- 15 已正常的方式终止一项任务  
>- 19 相当于键盘输入Ctrl-z暂停一个进程  

>ps 将某个时间点的进程摘取出来  
>- ps -l 查看自己bash下的进程   
>- ps aux 查看系统的所有进程  

